# Gmail Client Folders

Automatically apply labels to client emails, Sorting them into a "Clients" folder then a subfolder with the appropriate client label.

- Optionally sort "Messages" and "Services" within the clients individual folder.

### Setup

Create a google sheet with the following columns
```
ID, Label, Domains
```
select range "A2" and paste ```=ArrayFormula(LOWER(SUBSTITUTE(B2:B," ","")))``` push `cmd+shift+enter` to apply it to the entire column.

Save the sheet, and copy the ID in the url path ( we'll need this in a moment ).
```
https://docs.google.com/spreadsheets/d/{COPY_THIS_ID}/edit#gid=0
```

Goto [https://script.google.com/home](https://script.google.com/home).

Click "New Script"

Copy the contents of "Code.gs" from this repo into the new script ( replace all ).

Add the Sheet ID to the settings at the top of the script ( adjust any other settings accordingly ).

push "cmd+s" to save.

Goto [https://script.google.com/home/triggers](https://script.google.com/home/triggers).

Click "Create Trigger"

Set "function to run" as "processEmails".

Set "event source" to "Time-Driven"

Set "type of time based trigger" to "Minutes Timer"

Set "minute interval" to "Every minute"

Click "Save".

This should now run every minute, alternatively you can open the script project and click run to get started right away.