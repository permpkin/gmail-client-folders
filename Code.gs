var clientsDomain = 'support@mydomain.com';

var clientFolder = "Clients";
  
// ( if true ) add to clientFolder as well as the clientFolder/{ClientID}
var addToRootClientFolder = true;
  
// ( if false ) generate additional client sub folders for messages/services
var flattenClientFolders = true;

// ( if true ) mail will move straight to client folder and NOT appear in inbox.
var skipInbox = false;
  
// from https://docs.google.com/spreadsheets/d/{THIS_IS_THE_ID}/edit#gid=0
var documentId = "ENTER_SHEETS_ID";

function processEmails() {
  
  // get filtered messages.
  var filtered = GmailApp.search(`to:${clientsDomain} has:nouserlabels`);
  
  // filter newest items first.
  filtered.reverse().forEach(function(thread){
    
    // get first message in thread.
    var message = thread.getMessages()[0];
    
    var toAddress = message.getTo();
    
    // match client filter.
    var labelMatch = toAddress.match(/(?:\+)+(\w)+(?=\@(\w)(.))/g);
    
    var fromAddress = message.getFrom();
    
    var fromDomain = fromAddress.match(/@([A-Za-z0-9.-]+\.[A-Za-z]{2,4})/g)[0].slice(1);
    
    // check if to address contains client filter.
    if (labelMatch.length > 0) {
        
      var clientId = labelMatch[0].slice(1);
      
      // updateClientActivity(clientId);
      
      lookupClientDetail(clientId, fromDomain, function (clientData) {
        
        var labelArray = [clientFolder, clientData.label];
  
        var label = createNestedGmailLabel(labelArray.join('/'));
      
        // add label to thread.
        thread.addLabel(label);
        
        if (!flattenClientFolders) {
        
          var labelAffix = clientData.event+'s';
          
          var sublabelArray = [clientFolder, clientData.label, labelAffix];
  
          var subLabel = createNestedGmailLabel(sublabelArray.join('/'));
      
          // add sub label to thread.
          thread.addLabel(subLabel);
          
        };
        
        if (addToRootClientFolder) {
  
          var rootLabel = createNestedGmailLabel(clientFolder);
      
          // add sub label to thread.
          thread.addLabel(rootLabel);
          
        };
        
      })
      
      // skip inbox
      if (skipInbox) {
       thread.moveToArchive();
      };
        
    };
    
  });
  
}

function createNestedGmailLabel(name) {
  
  var labels = name.split("/");
  var gmail, label = "";
  
  for (var i=0; i<labels.length; i++) {
    
    if (labels[i] !== "") {
      label = label + ((i===0) ? "" : "/") + labels[i];
      gmail = GmailApp.getUserLabelByName(label) ? 
                  GmailApp.getUserLabelByName(label) : GmailApp.createLabel(label);
    }
  }
  
  return gmail;
  
}

function getDataFromId(source, str, fromDomain) {
  
  for (var i = 0; i < source.length; i++) {
    var row = source[i];
    if (row[0] == str) {
      return { label:row[1], domains: row[2].split(','), event: ((row[2].split(',').indexOf(fromDomain) < 0)?'Service':'Message') };
    }
  }
  
}

function lookupClientDetail(clientId, fromDomain, callback) {
  
  var spreadsheet = SpreadsheetApp.openById(documentId);
    
  var sheet = spreadsheet.getSheets()[0];
  
  var columnValues = sheet.getRange(2, 1, sheet.getLastRow(), 3).getValues();
  
  var clientData = getDataFromId(columnValues, clientId, fromDomain);
  
  updateClientActivity(spreadsheet, clientId, clientData, fromDomain);
  
  callback(clientData);
    
}

function updateClientActivity(spreadsheet, clientId, clientData, fromDomain) {
    
  var sheet = spreadsheet.getSheets()[1];
  
  sheet.appendRow([clientId, fromDomain, clientData.event.toLowerCase(), new Date]);
    
}